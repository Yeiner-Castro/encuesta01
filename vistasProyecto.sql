CREATE VIEW view_Mujeres
as select distinct u.Apds_usuario, u.Edad, e.nomb_encuesta From
Encu_Preg_Resp_Usu t
join Usua_Resp ur on ur.ID_Usuario=t.ID_Usuario
join Usuario u on u.ID_Usuario=ur.ID_Usuario
join Encuesta_Pregunta ep on ep.ID_Encuesta=t.ID_encuesta
join Encuesta e on e.ID_Encuesta=ep.ID_Encuesta
WHERE  u.sexo='f' AND e.id_encuesta='1';

CREATE VIEW view_Hombres
as select distinct u.Apds_usuario, u.Edad, e.nomb_encuesta From
Encu_Preg_Resp_Usu t
join Usua_Resp ur on ur.ID_Usuario=t.ID_Usuario
join Usuario u on u.ID_Usuario=ur.ID_Usuario
join Encuesta_Pregunta ep on ep.ID_Encuesta=t.ID_encuesta
join Encuesta e on e.ID_Encuesta=ep.ID_Encuesta
WHERE  u.sexo='m' AND e.id_encuesta='1';

CREATE VIEW view_menoresDeEdad
as select distinct u.Apds_usuario, e.nomb_encuesta From
Encu_Preg_Resp_Usu t
join Usua_Resp ur on ur.ID_Usuario=t.ID_Usuario
join Usuario u on u.ID_Usuario=ur.ID_Usuario
join Encuesta_Pregunta ep on ep.ID_Encuesta=t.ID_encuesta
join Encuesta e on e.ID_Encuesta=ep.ID_Encuesta
WHERE  u.edad<'18' AND e.id_encuesta='1';

CREATE VIEW view_preguntas_respuestas
as select distinct u.nombs_usuario, p.pre_texto, r.resp, e.nomb_encuesta From
Encu_Preg_Resp_Usu t
join Usua_Resp ur on ur.ID_Usuario=t.ID_Usuario
join Usua_Resp ur1 on ur1.ID_Resp=t.ID_Resp
join Usuario u on u.ID_Usuario=ur.ID_Usuario
join Respuesta r on r.ID_resp=ur1.ID_resp
join Encuesta_Pregunta ep on ep.ID_Pregunta=t.ID_Pregunta
join Encuesta_Pregunta ep1 on ep1.ID_Encuesta=t.ID_Encuesta
join Pregunta p on p.ID_Pregunta=ep.ID_Pregunta
join Encuesta e on e.ID_Encuesta=ep1.ID_Encuesta;


